// This is a generated file, do not edit
window._env_ = {
  REACT_APP_SERVICE_NAME: 'ÅNPLAYER',
  REACT_APP_BASE_URL: 'https://listen.whatsthis.one',
  REACT_APP_STREAM_MGR: 'https://listen.whatsthis.one/mgr/streaming/',
  REACT_APP_STREAM_SERVICE: 'https://stream.whatsthis.one',
  REACT_APP_USER_SERVICE: 'https://listen.whatsthis.one/user/',
  REACT_APP_PLAYER_WATERMARK: 'http://assets.whatsthis.one.s3.wasabisys.com/anplayer/an-glitch.gif',
  REACT_APP_PLAYER_POSTER_VIDEO: 'https://assets.whatsthis.one/anplayer/poster-video.jpg',
  REACT_APP_PLAYER_POSTER_AUDIO: 'https://assets.whatsthis.one/anplayer/poster-audio.jpg',
  REACT_APP_PLAYER_POSTER_OFFLINE: 'https://assets.whatsthis.one/anplayer/offline-glitch.gif',
};

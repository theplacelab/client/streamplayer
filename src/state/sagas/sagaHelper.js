import { API } from "constants.js";

export const makeRequest = (api, method, url, body, isSigned) => {
  let baseUrl;
  switch (api) {
    case API.STREAM_MANAGER:
      baseUrl = window._env_.REACT_APP_STREAM_MGR;
      break;
    case API.USER_MANAGER:
      baseUrl = window._env_.REACT_APP_USER_SERVICE;
      break;
    default:
      baseUrl = null;
  }
  url = `${baseUrl}${url}`;
  if (isSigned) {
    return signed(method, url, body);
  } else {
    return anonymous(method, url, body);
  }
};

const signed = (method, url, body = {}) => {
  const token = localStorage.getItem("authtoken");
  let request = {
    method: method,
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/json; charset=utf-8",
      Prefer: method === "POST" ? "return=representation" : "",
    },
    body:
      typeof body === "string"
        ? body
        : JSON.stringify(body).replace(/\\u0000/g, ""),
  };
  if (!body || method === "GET") delete request.body;
  return fetch(url, request);
};

const anonymous = (method, url, body) => {
  let request = {
    method: method,
    headers: {
      Accept: "application/json, application/xml, text/plain, text/html, *.*",
      "Content-Type": "application/json; charset=utf-8",
    },
    body: typeof body === "string" ? body : JSON.stringify(body),
  };
  if (!body || method === "GET") delete request.body;
  return fetch(url, request);
};

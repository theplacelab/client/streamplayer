import * as actions from "state/actions";
import { put, call, delay, select } from "redux-saga/effects";
import { API } from "constants.js";
import { makeRequest } from "./sagaHelper.js";
import { decodeToken } from "utility.js";

export const periodicallyReauthorize = function* () {
  const reduxState = yield select();
  const currentToken = localStorage.getItem("authtoken");
  while (true) {
    if (reduxState.user.isAuthenticated) {
      let now = Date.now() / 1000;
      let secondsUntilExpire = reduxState.user.claims.exp - now;
      if (secondsUntilExpire <= 120) {
        yield call(reauthorize);
      }
    } else {
      if (currentToken) yield call(reauthorize);
    }
    yield delay(60000);
  }
};

export const reauthorize = function* (action) {
  const response = yield makeRequest(
    API.USER_MANAGER,
    "POST",
    `rpc/reauthorize`,
    { token: localStorage.getItem("authtoken") },
    true
  );
  if (response.ok) {
    const res = yield response.json();
    const token = res[0].token;
    localStorage.setItem("authtoken", token);
    yield put({
      type: actions.USER_LOGIN_RESPONSE,
      payload: { isAuthenticated: true, token, claims: decodeToken(token) },
    });
  } else {
    localStorage.removeItem("authtoken");
    console.warn("Reauthorization Failed");
    yield call(logout);
  }
};

export const login = function* (action) {
  localStorage.removeItem("authtoken");
  const response = yield makeRequest(
    API.USER_MANAGER,
    "POST",
    `rpc/login`,
    { email: action.payload.username, pass: action.payload.password },
    false
  );
  if (response.ok) {
    const res = yield response.json();
    const token = res[0].token;
    localStorage.setItem("authtoken", token);
    yield put({
      type: actions.USER_LOGIN_RESPONSE,
      payload: { isAuthenticated: true, token, claims: decodeToken(token) },
    });
  } else {
    console.warn("Login Failed");
    yield call(logout);
  }
};

export const logout = function* (action) {
  localStorage.removeItem("authtoken");
  yield put({
    type: actions.USER_LOGIN_RESPONSE,
    payload: {
      isAuthenticated: false,
      token: "",
      claims: {},
    },
  });
};

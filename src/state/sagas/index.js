import { takeLatest, all, call } from "redux-saga/effects";
import * as actions from "state/actions";
import * as stream from "./stream.js";
import * as user from "./user.js";

export default function* rootSaga() {
  yield all([
    user.periodicallyReauthorize(),
    stream.periodicallyCheckStatus(),
    takeLatest(actions.GET_STREAM_STATUS, function* (action) {
      yield call(stream.getStatus, action.payload);
    }),
    takeLatest(actions.USER_LOGIN, user.login),
    takeLatest(actions.USER_LOGOUT, user.logout),
    takeLatest(actions.GET_STREAM_KEYS, stream.getKeys),
  ]);
}

import * as actions from "state/actions";
import { put } from "redux-saga/effects";

export const sagaError = function* (action) {
  yield put({ type: actions.UI_SHOW_FATAL_ERROR, payload: action.payload });
  console.log(action);
  yield console.error(
    `😫 SAGA FATAL (${JSON.stringify(action.payload.source)}): ${
      action.payload.error
        ? JSON.stringify(action.payload.error.response)
        : "UNKNOWN"
    }`
  );
};

export const sagaErrorNonfatal = function* (action) {
  yield console.warn(
    `😫 SAGA NONFATAL ERROR (${action.payload.source}): ${
      action.payload.error ? action.payload.error : "UNKNOWN"
    }`
  );
};

export const log = (effect, action) => {
  console.warn(
    `😫 SAGA LOG (${action.payload.source}): ${
      action.payload.error ? action.payload.error : "UNKNOWN"
    }`
  );
  return effect;
};

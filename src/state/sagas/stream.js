import { put, call, delay } from "redux-saga/effects";
import * as helper from "./sagaHelper.js";
import * as actions from "state/actions";
import { API } from "constants.js";

export const periodicallyCheckStatus = function* () {
  while (true) {
    yield call(getStatus);
    yield delay(10000);
  }
};

export const getStatus = function* (action) {
  const response = yield helper.makeRequest(API.STREAM_MANAGER, "GET", `info`);
  if (response.ok) {
    const res = yield response.json();
    yield put({
      type: actions.STREAM_STATUS_RESPONSE,
      payload: res,
    });
  } else {
    yield put({
      type: actions.STREAM_STATUS_RESPONSE,
      payload: {},
    });
  }
};

export const getKeys = function* (action) {
  const response = yield helper.makeRequest(
    API.STREAM_MANAGER,
    "GET",
    `setup/${action.payload.streamName}`,
    null,
    true
  );
  if (response.ok) {
    const res = yield response.json();
    yield put({
      type: actions.STREAM_KEYS_RESPONSE,
      payload: { ...res, name: action.payload.streamName },
    });
  } else {
    yield put({
      type: actions.STREAM_KEYS_RESPONSE,
      payload: {},
    });
  }
};

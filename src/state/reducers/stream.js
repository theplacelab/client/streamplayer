import initialState from "./init/stream";
import * as actions from "state/actions";

const stream = (state = initialState, action) => {
  switch (action.type) {
    case actions.STREAM_STATUS_RESPONSE:
      return { ...state, serverStatus: action.payload };
    case actions.STREAM_KEYS_RESPONSE:
      let keys = { ...state.keys };
      keys[action.payload.name] = action.payload;
      return { ...state, keys };
    default:
      return state;
  }
};

export default stream;

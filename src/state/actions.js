function action(type, payload = {}) {
  return { type, payload };
}

export const GET_STREAM_STATUS = "GET_STREAM_STATUS";
export const STREAM_STATUS_RESPONSE = "STREAM_STATUS_RESPONSE";

export const GET_STREAM_KEYS = "GET_STREAM_KEYS";
export const STREAM_KEYS_RESPONSE = "STREAM_KEYS_RESPONSE";
export const getStreamKeys = (payload) => action(GET_STREAM_KEYS, payload);

export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGIN_RESPONSE = "USER_LOGIN_RESPONSE";
export const USER_LOGOUT = "USER_LOGOUT";
export const userLogin = (payload) => action(USER_LOGIN, payload);
export const userLogout = () => action(USER_LOGOUT);

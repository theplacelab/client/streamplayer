import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CopyToClipboard from "components/CopyToClipboard";

function StreamInfo({ stream, includeLink }) {
  if (!stream) return null;
  const streamLink = `${window._env_.REACT_APP_STREAM_SERVICE}/video/${stream.name}.m3u8`;

  const audioOnly =
    parseInt(stream.meta?.video.width, 10) === 0 &&
    parseInt(stream.meta?.video.height, 10) === 0;
  return (
    <div key={stream.name}>
      <div
        style={{
          textAlign: "left",
          display: "flex",
          flexDirection: "row",
          marginBottom: "1rem",
        }}
      >
        <div style={{ marginRight: "1rem", marginTop: "0.2rem" }}>
          <FontAwesomeIcon icon={audioOnly ? "headphones-alt" : "tv"} />
        </div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div>
            <Link
              style={{
                color: "white",
                textDecoration: "none",
                fontWeight: "900",
              }}
              to={`/watch/${stream.name}`}
            >
              {stream.name}
            </Link>
          </div>
          <div style={{ fontSize: "1rem", textAlign: "left" }}>
            {!audioOnly && (
              <div>
                {stream.meta.video.width}x{stream.meta.video.height}{" "}
                {stream.meta.video.frame_rate}fps | {stream.meta.video.codec} |{" "}
                {stream.meta.video.level}
              </div>
            )}
            <div>
              {stream.meta.audio.channels} channel | {stream.meta.audio.codec} |{" "}
              {stream.meta.audio.sample_rate}
              Hz
            </div>
            <div>
              <CopyToClipboard content={streamLink}>
                {streamLink}
              </CopyToClipboard>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default StreamInfo;

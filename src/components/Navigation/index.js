import React from "react";
import { useSelector } from "react-redux";
import Login from "components/user/Login";
import { Link } from "react-router-dom";

function Navigation() {
  const user = useSelector((redux) => redux.user);

  return (
    <div style={styles.container}>
      <div style={{ width: "32vw", overflow: "hidden", marginRight: "1rem" }}>
        {window._env_.REACT_APP_SERVICE_NAME || "STREAMPLAYER"}
      </div>
      <div style={{ flexGrow: 1 }}></div>
      <div
        style={{
          textAlign: "right",
          display: "flex",
          flexDirection: "row",
          marginRight: "1rem",
        }}
      >
        {user.isAuthenticated && (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              marginRight: "1rem",
              flexGrow: 1,
            }}
          >
            <div>
              <Link style={styles.link} to="/streams">
                Watch
              </Link>
            </div>
            <div style={{ padding: "0 1rem 0 1rem" }}>•</div>
            <div>
              <Link style={styles.link} to="/broadcast">
                Broadcast
              </Link>
            </div>
          </div>
        )}
        <div style={{ marginRight: ".5rem" }}>
          <Login />
        </div>
      </div>
    </div>
  );
}
export default Navigation;

const styles = {
  container: {
    backgroundColor: "#222222",
    position: "fixed",
    width: "100%",
    textAlign: "left",
    height: "3rem",
    padding: "0 1rem 0 1rem",
    display: "flex",
    flexDirection: "row",
    fontSize: "1.5rem",
    alignItems: "center",
  },
  link: {
    fontSize: "1rem",
    color: "white",
    textDecoration: "none",
    textTransform: "uppercase",
    fontWeight: 900,
  },
};

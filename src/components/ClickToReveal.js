import React, { useState } from "react";

function ClickToReveal({ children }) {
  const [showContent, setShowContent] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
      }}
    >
      <div onClick={() => setShowContent(!showContent)}>
        {(showContent && children) || (
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <div>*******************</div>
            <div>(click to reveal)</div>
          </div>
        )}
      </div>
    </div>
  );
}
export default ClickToReveal;

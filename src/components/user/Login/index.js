import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as actions from "state/actions";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const user = useSelector((redux) => redux.user);
  const dispatch = useDispatch();

  const onLogin = (e) => {
    e.preventDefault();
    dispatch(actions.userLogin({ username, password }));
    setUsername("");
    setPassword("");
  };

  const onLogout = (e) => {
    if (window.confirm("Do you really want to log out?"))
      dispatch(actions.userLogout());
  };

  if (user.isAuthenticated) {
    return (
      <div style={styles.containerAuth}>
        <div onClick={onLogout} style={styles.userAvatar}>
          {user.claims.name.charAt(0)}
        </div>
      </div>
    );
  } else {
    return (
      <form onSubmit={onLogin}>
        <div style={styles.container}>
          <div>
            <input
              autoComplete="current-username"
              value={username}
              style={styles.input}
              type="text"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div>
            <input
              autoComplete="current-password"
              value={password}
              style={styles.input}
              type="password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <input style={styles.input} type="submit" value="Login" />
          </div>
        </div>
      </form>
    );
  }
}
export default Login;

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  containerAuth: {
    display: "flex",
    flexDirection: "row",
    padding: "0",
  },
  input: {
    width: "10rem",
    fontSize: "1rem",
    height: "2rem",
    border: 0,
    marginRight: ".1rem",
    borderRadius: "0.3rem",
  },
  link: {
    color: "purple",
    textDecoration: "underline",
  },
  userAvatar: {
    border: "2px solid ",
    borderRadius: "3rem",
    height: "2rem",
    width: "2rem",
    lineHeight: "2rem",
    overflow: "hidden",
    fontWeight: "900",
    backgroundColor: "#d14787",
    textAlign: "center",
  },
};

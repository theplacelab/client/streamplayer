import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Clappr from "clappr";

const watermark = window._env_.REACT_APP_PLAYER_WATERMARK || "/watermark.gif";
const poster_video =
  window._env_.REACT_APP_PLAYER_POSTER_VIDEO || "/poster_video.png";
const poster_audio =
  window._env_.REACT_APP_PLAYER_POSTER_AUDIO || "/poster_audio.png";
const poster_offline =
  window._env_.REACT_APP_PLAYER_POSTER_OFFLINE || "/poster_offline.png";

let player;
let playerContainer;
const ClapprPlayer = ({ source, audioOnly = false }) => {
  useEffect(() => {
    fetch(source)
      .then((response) => {
        if (response.ok) {
          if (player) player.destroy();
          player = new Clappr.Player({
            parent: playerContainer,
            autoPlay: true,
            watermark,
            poster: audioOnly ? poster_audio : poster_video,
            source,
            width: "100%",
            height: "100%",
            events: {
              onStop: () => {
                if (player) {
                  console.log("onStop!");
                  player.stop();
                }
              },
              onPause: () => {
                if (player) {
                  console.log("OnPause!");
                  player.pause();
                }
              },
              onError: () => {
                if (player) {
                  console.log("OnError!");
                  player.stop();
                }
              },
              onEnded: () => {
                if (player) {
                  console.log("OnEnded!");
                  player.play();
                }
              },
            },
          });
          console.log(player);
        } else {
          console.error(source);
        }
      })
      .catch(() => {
        console.error(source);
      });
    return () => {
      if (player) player.destroy();
      player = null;
    };
  }, [audioOnly, source]);

  return (
    <div>
      <div
        style={{
          height: `35rem`,
          maxHeight: "35rem",
          background: `#000000 url('${poster_offline}')`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "contain",
          backgroundPosition: "center center",
        }}
        id="clapprPlayer"
        ref={(el) => (playerContainer = el)}
      ></div>
    </div>
  );
};
export default ClapprPlayer;

ClapprPlayer.defaultProps = {
  isBorderless: false,
};
ClapprPlayer.propTypes = {
  source: PropTypes.string,
};

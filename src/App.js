import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Stream from "pages/Stream";
import Broadcast from "pages/Broadcast";
import LiveStreams from "pages/LiveStreams";
import { Provider } from "react-redux";
import store from "store";
import Navigation from "components/Navigation";
import PrivateRoute from "components/user/PrivateRoute";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faFileAudio,
  faVideo,
  faCheckSquare,
  faCopy,
  faHeadphonesAlt,
  faTv,
  faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faSignOutAlt,
  faTv,
  faFileAudio,
  faVideo,
  faCheckSquare,
  faCopy,
  faHeadphonesAlt
);
function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navigation></Navigation>
          <Switch>
            <Route path="/watch/:id" component={Stream} />
            <Route path="/watch" component={LiveStreams} />
            <PrivateRoute path="/broadcast" component={Broadcast} />{" "}
            <Route path="/" component={LiveStreams} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;

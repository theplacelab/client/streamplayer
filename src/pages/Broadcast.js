import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getStreamKeys } from "state/actions";
import CopyToClipboard from "../components/CopyToClipboard";
import ClickToReveal from "../components/ClickToReveal";

const ListCredentials = ({ credentials }) => {
  const styles = {
    row: {
      display: "flex",
      flexDirection: "row",
      padding: "0 1rem",
      margin: "1rem 0 1rem 0",
      alignItems: "center",
    },
    headerRow: {
      display: "flex",
      flexDirection: "row",
      padding: "0",
      margin: "1rem",
      height: "2rem",
    },
    header: {
      fontWeight: 900,
      width: "15rem",
      textAlign: "left",
    },
    credentialBox: {
      backgroundColor: "#5aa4b0",
      padding: "1rem",
      borderRadius: "0 0 0.3rem 0.3rem",
      fontSize: "1rem",
    },
    headerBlock: {
      display: "flex",
      flexDirection: "column",
      backgroundColor: "#6a6a65",
      height: "4rem",
    },
  };
  let list = [];
  Object.keys(credentials).forEach((name) => {
    const stream = credentials[name];

    const linkText =
      "Keep Secure:\n" +
      "------------\n" +
      `Server: ${stream.broadcast_SERVICE} \n` +
      `Key: ${stream.broadcast_key}\n\n` +
      "Sharable:\n" +
      "-----------\n" +
      `Stream URL: ${stream.view_hls}\n` +
      `Web Player: ${window._env_.REACT_APP_BASE_URL}/watch/${stream.name} `;

    list.push(
      <div
        key={name}
        style={{ margin: "1rem", display: "flex", flexDirection: "column" }}
      >
        <div style={styles.headerBlock}>
          <div style={styles.headerRow}>
            <div style={styles.header}>{stream.name}</div>
            <div style={{ flexGrow: 1 }} />
            <div
              style={{ textAlign: "right", width: "2rem", lineHeight: "2rem" }}
            >
              <CopyToClipboard content={linkText} />
            </div>
          </div>
          <div style={styles.credentialBox}>
            <div style={styles.row}>
              <div style={styles.header}>Server (Private):</div>
              <div>
                <CopyToClipboard>{stream.broadcast_SERVICE}</CopyToClipboard>
              </div>
            </div>
            <div style={styles.row}>
              <div style={styles.header}>Stream Key (Private):</div>
              <div>
                <CopyToClipboard content={stream.broadcast_key}>
                  <ClickToReveal>{stream.broadcast_key}</ClickToReveal>
                </CopyToClipboard>
              </div>
            </div>
            <div style={styles.row}>
              <div style={styles.header}>Stream URL (Public):</div>
              <div>
                <CopyToClipboard>{stream.view_hls}</CopyToClipboard>
              </div>
            </div>
            <div style={styles.row}>
              <div style={styles.header}>Web Player (Public):</div>
              <div>
                <CopyToClipboard>
                  {`${window._env_.REACT_APP_BASE_URL}/watch/${stream.name}`}
                </CopyToClipboard>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  });
  return list;
};

function Credentials() {
  const [streamName, setStreamName] = useState(" ");
  const dispatch = useDispatch();
  const onSetup = (e) => {
    if (streamName.trim().length > 0) {
      dispatch(getStreamKeys({ streamName: streamName.trim() }));
      setStreamName("");
    }
  };
  const onChange = (e) => {
    let val = e.target.value;
    const re = /[a-z-]+/g;
    setStreamName(
      val.toLowerCase().match(re)?.join("")
        ? val.toLowerCase().match(re)?.join("")
        : ""
    );
  };
  const streamCredentials = useSelector((redux) => redux.stream.keys);
  return (
    <div
      className="page"
      style={{
        padding: "5rem 2rem",
        textAlign: "left",
        fontSize: "2rem",
        maxWidth: "60rem",
        margin: "auto",
        fontWeight: 200,
      }}
    >
      Broadcast
      <div style={{ borderTop: "1px solid white", paddingTop: "1rem" }}>
        <div>Type in the name of the stream you want.</div>
        <div>
          <ul>
            <li style={{ marginBottom: "1rem" }}>
              Names should be all lowercase and include only lower-case letters
              and hyphens.
            </li>
            <li style={{ marginBottom: "1rem" }}>
              Stream must be started within 24 hours of credentials being
              issued. Streams can last for any duration.
            </li>
            <li style={{ marginBottom: "1rem" }}>
              This page is not persistent! You should copy credentials you want
              to keep. If you lose them, just issue new ones.
            </li>
          </ul>
        </div>
      </div>
      <div
        style={{
          marginLeft: "1rem",
          textAlign: "center",
        }}
      >
        <input
          style={styles.input}
          styles={{ width: "10rem" }}
          type="text"
          onChange={onChange}
          value={streamName}
        />
        <input
          onClick={onSetup}
          style={styles.input}
          type="button"
          value="Generate Keys"
        />
      </div>
      <div>
        <ListCredentials credentials={streamCredentials} />
      </div>
    </div>
  );
}
export default Credentials;

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  containerAuth: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "grey",
    borderRadius: "0.3rem",
  },
  input: {
    border: 0,
    marginRight: "1rem",
    borderRadius: "0.3rem",
    minWidth: "10rem",
    fontSize: "2rem",
  },
  link: {
    color: "purple",
    textDecoration: "underline",
  },
};

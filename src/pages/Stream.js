import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import ClapprPlayer from "components/ClapprPlayer";
import StreamInfo from "../components/StreamInfo";

function Stream() {
  let { id } = useParams();
  let streamData;
  let audioOnly;

  // Attempt to parse the info block, try/catch is a bit lazy but such a complicated object :P
  try {
    const streamStatus = useSelector(
      (redux) => redux.stream?.serverStatus?.server?.application
    );
    const live = streamStatus.find((item) => item.name === "live");
    const streamArray = Array.isArray(live.live.stream)
      ? live.live.stream
      : [live.live.stream];
    streamData = streamArray.find((stream) => stream.name === id);
    audioOnly =
      parseInt(streamData.meta.video.width, 10) === 0 &&
      parseInt(streamData.meta.video.height, 10) === 0;
  } catch {}

  const streamLink = `${window._env_.REACT_APP_STREAM_SERVICE}/video/${id}.m3u8`;

  return (
    <div style={{ paddingTop: "3rem", textAlign: "left" }}>
      <ClapprPlayer source={streamLink} audioOnly={audioOnly} />
      <div style={{ padding: "1rem", fontSize: "2rem" }}>
        <StreamInfo stream={streamData} includeLink={true} />
      </div>
    </div>
  );
}
export default Stream;

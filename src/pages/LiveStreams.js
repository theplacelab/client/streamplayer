import React from "react";
import { useSelector } from "react-redux";
import StreamInfo from "../components/StreamInfo";

const Streams = ({ streams }) => {
  let list = [];
  if (streams && streams.length > 0) {
    streams.forEach((stream) => {
      if (stream) list.push(<StreamInfo stream={stream} />);
    });
  }
  return list;
};
function LiveStreams() {
  const serverState = useSelector((redux) => redux.stream.serverStatus);
  let streams = [];

  try {
    if (serverState.server) {
      const live = serverState.server.application.find(
        (item) => item.name === "live"
      );
      streams = Array.isArray(live.live.stream)
        ? live.live.stream
        : live.live.stream
        ? [live.live.stream]
        : null;
    }
  } catch {
    console.error("No Stream Status");
  }

  return (
    <div
      className="page"
      style={{
        padding: "5rem 2rem",
        textAlign: "left",
        fontSize: "2rem",
        maxWidth: "60rem",
        margin: "auto",
        fontWeight: 200,
      }}
    >
      Watch
      <div style={{ borderTop: "1px solid white", paddingTop: "1rem" }}>
        {(streams?.length > 0 && <Streams streams={streams} />) ||
          "No Streams Online"}
      </div>
    </div>
  );
}
export default LiveStreams;

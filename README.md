# NGINX Streamer: Client

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

Create-React-App based SPA that provides a UI for the manager-server so you can see and manage streams.

Built on top of:
https://gitlab.com/theplacelab/service/login
https://gitlab.com/theplacelab/service/streamer

- This project makes use of FontAwesome icons under the [Creative Commons Attribution 4.0 International license](https://fontawesome.com/license)
